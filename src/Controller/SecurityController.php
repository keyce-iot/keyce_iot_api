<?php

namespace App\Controller;

use App\Entity\Alert;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController extends AbstractController
{

    private EntityManagerInterface $em;
    private MailerInterface $mailerInterface;

    public function __construct(EntityManagerInterface $em, MailerInterface $mailerInterface)
    {
        $this->em = $em;
        $this->mailerInterface = $mailerInterface;
    }


    /**
     * @Route("/api/current-user", methods={"GET"})
     */
    public function currentUser(SerializerInterface $serializer): Response
    {
        $data = '';
        try {
            $user = $this->getUser();
            $data = $serializer->serialize($user, JsonEncoder::FORMAT);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

        return new Response($data);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/api/login/lost-password", methods={"POST"})
     */
    public function lostPassword(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        try {
            $datasPersonne = $request->toArray();
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $datasPersonne['email']]);
            $passwordRandom = $this->RandomString(10);
            $this->sendMaillostPassword($passwordRandom);
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $passwordRandom
            );
            $user->setPassword($hashedPassword);
            $this->em->persist($user);
            $this->em->flush();

            return new Response('Mot de passe modifié', '200');
        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

    }

    public function sendMaillostPassword(string $password): Response
    {
        try {
            $email = (new Email())
                ->from('hello@example.com')
                ->to('you@example.com')
                ->subject('Nouveau mot de passe')
                ->text('Voici votre nouveau mot de passe : ' . $password . '. Pour plus de sécurité veuillez le modifier s\'il vous plait !');
            $this->mailerInterface->send($email);

            return $this->render('mail/index.html.twig', [
                'controller_name' => 'MailController',
            ]);
        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    function RandomString($length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
