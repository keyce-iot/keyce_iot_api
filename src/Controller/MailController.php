<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailController extends AbstractController
{

    private MailerInterface $mailerInterface;

    public function __construct(MailerInterface $mailerInterface)
    {
        $this->mailerInterface = $mailerInterface;
    }

    /**
     * @Route("/api/mail", name="mail")
     */
    public function index(Request $request): Response
    {
        $alert = $request->toArray();
        dump($alert);
        if (isset($alert['alert']['temperatureCapteur'])) {
            $email = (new Email())
                ->from('hello@example.com')
                ->to('you@example.com')
                ->subject('Une alerte de température pour "' . $alert['alert']['name'] . '" vient d\'être levée')
                ->text('Les températures min/max devraient être de : ' . $alert['alert']['tempMin'] . '/' . $alert['alert']['tempMax'] . ' et le capteur à relevé une température de ' . $alert['alert']['temperatureCapteur']);
            $this->mailerInterface->send($email);
        }
        if (isset($alert['alert']['humidityCapteur'])) {
            $email = (new Email())
                ->from('keyce@e-cdp.com')
                ->to('you@example.com')
                ->subject('Une alerte d\'humidité pour "' . $alert['alert']['name'] . '" vient d\'être levée')
                ->text('L\'humidité min/max devraient être de : ' . $alert['alert']['humMin'] . '/' . $alert['alert']['humMax'] . ' et le capteur à relevé une température de ' . $alert['alert']['humidityCapteur']);
            $this->mailerInterface->send($email);
        }

        return $this->render('mail/index.html.twig', [
            'controller_name' => 'MailController',
        ]);
    }
}
