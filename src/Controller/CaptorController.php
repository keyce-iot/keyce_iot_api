<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CaptorRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class CaptorController extends AbstractController
{
    /**
     * @Route("/api/datas/capteur")
     */
    public function getAllData(SerializerInterface $serializer, CaptorRepository $repo): Response
    {
        $data = '';
        try {
            $captor = $repo->findAll();
            $data = $serializer->serialize($captor, JsonEncoder::FORMAT);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
