<?php

namespace App\Controller;

use App\Entity\Alert;
use App\Repository\AlertRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class AlertController extends AbstractController
{

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/api/datas/alert", methods={"GET"})
     */
    public function getAllData(SerializerInterface $serializer, AlertRepository $repo): Response
    {
        $data = '';
        try {
            $alert = $repo->findAll();
            $data = $serializer->serialize($alert, JsonEncoder::FORMAT);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

        return new Response($data);
    }

    /**
     * @Route("/api/datas/alert", methods={"POST"})
     */
    public function createAlert(SerializerInterface $serializer, Request $request): Response
    {
        try {
            $dataAlert = $request->toArray();
            $alert = new Alert();
            $this->extracted($alert, $dataAlert['alert']);
            $alert->setDateCreation(new \DateTime());

            $this->em->persist($alert);
            $this->em->flush();//

            $response = new Response($serializer->serialize($alert, 'json'));
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;

        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @Route("/api/datas/alert/{id}", methods={"POST"})
     */
    public function deleteAlert(Request $request): Response
    {
        try {
        $idAlert = $request->toArray();
        $alert = $this->em->getRepository(Alert::class)->findOneBy(['id' => $idAlert['alertId']]);
        $this->em->remove($alert);
        $this->em->flush();

        return new JsonResponse($idAlert['alertId'], 200);

        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @Route("/api/datas/alert", methods={"PUT"})
     */
    public function updateAlert(Request $request): Response
    {
        $dataAlert = $request->toArray();
        try {
            $alert = $this->em->getRepository(Alert::class)->findOneBy(['id' => $dataAlert['alert']['id']]);
            $this->extracted($alert, $dataAlert['alert']);
            $alert->setDateModification(new \DateTime());

            $this->em->persist($alert);
            $this->em->flush();

            return new JsonResponse($dataAlert['alert']['id'], 200);

        } catch (Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param mixed $alert
     * @param $key
     * @return void
     */
    public function extracted(mixed $alert, $key): void
    {
        $alert->setName($key['name']);
        $alert->setTempMin($key['tempMin']);
        $alert->setTempMax($key['tempMax']);
        $alert->setHumMin($key['humMin']);
        $alert->setHumMax($key['humMax']);
        $alert->setActive($key['actif']);
    }
}
