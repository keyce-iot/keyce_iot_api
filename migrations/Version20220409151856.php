<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220409151856 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur CHANGE device device VARCHAR(255) NOT NULL, CHANGE bat_status bat_status VARCHAR(255) NOT NULL, CHANGE bat_level bat_level DOUBLE PRECISION NOT NULL, CHANGE temperature temperature DOUBLE PRECISION NOT NULL, CHANGE humidity humidity DOUBLE PRECISION NOT NULL, CHANGE payload payload VARCHAR(255) NOT NULL, CHANGE date_time date_time DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE capteur CHANGE device device VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE bat_status bat_status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE bat_level bat_level NUMERIC(3, 1) DEFAULT NULL, CHANGE temperature temperature NUMERIC(3, 1) DEFAULT NULL, CHANGE humidity humidity NUMERIC(3, 1) DEFAULT NULL, CHANGE payload payload VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE date_time date_time DATETIME DEFAULT NULL');
    }
}
